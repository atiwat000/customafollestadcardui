package com.afollestad.cardsui;

import android.content.Context;

public class CardCentered extends Card {

	public CardCentered(String title) {
		super(title);
	}

	public CardCentered(Context context, int title) {
		super(context, title);
	}

	@Override
	public int getLayout() {
		return R.layout.list_item_card_centeredtitle;
	}

	@Override
	public boolean isClickable() {
		return true;
	}

}